
public interface Pet {
	public abstract String getNumbered();
	public abstract String getType();
	public abstract String getVarieties();
	public abstract int getPrice();
	public abstract int getNumber();
	public abstract void setNumber(int number);
}
