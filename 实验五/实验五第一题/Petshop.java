
import java.util.Scanner;
class Petshop {
	Scanner in=new Scanner(System.in);
	private Pet[] p;
	private int count=0;//宠物的个数
	public Petshop(){
		
	}
	public Petshop(int len){//生成宠物数组
		if(len>0){
			p=new Pet[len];
		}
		else if(len==0){
			p=new Pet[1];			
		}
	}
	public boolean add(Pet pet){//增加宠物
		if(p.length>count){
			p[count]=pet;
			count++;
			return true;
		}
		else{
			return false;
		}
	}
	public void showPet(){//输出宠物信息
			for(int i=0;i<p.length;i++){
				System.out.println("编号:"+p[i].getNumbered()+"种类:"+p[i].getType()+"品种:"+
			p[i].getVarieties()+"单价:"+p[i].getPrice()+"剩余数量:"+p[i].getNumber());
			}
	}
	public void sub(){//购买宠物并输出购买列表
		int price=0;
		System.out.println("请输入购买宠物的编号,以空格隔开");
		String str=in.nextLine();
		String[] a=str.split(" ");
		for(int i=0;i<a.length;i++){//购买的宠物个数
			for(int j=0;j<p.length;j++){//遍历具有的宠物
				if(p[j].getNumbered().equals(a[i])){//查找到宠物
					p[j].setNumber(p[j].getNumber()-1);
					System.out.println("编号:"+p[j].getNumbered()+"种类:"+p[j].getType()+"品种:"+
			p[j].getVarieties()+"单价:"+p[j].getPrice()+"剩余数量:"+p[j].getNumber());
					price=price+p[j].getPrice();
					break;
				}
			}
		}
		System.out.println("合计"+price+"元");
	}
}
