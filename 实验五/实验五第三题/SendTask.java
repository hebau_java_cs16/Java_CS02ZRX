
public class SendTask {
	private String no;
	private float weight;
	public SendTask() {
		
	}
	public SendTask(String no,float weight) {
		this.no=no;
		this.weight=weight;
	}
	public void sendBefore(){
		System.out.println("运输前的单号为"+no+",重量为"+weight);
	}
	public void send(Transportation t,GPS tool) {
		t.transport();
		tool.showCoordinate();
	}
	public void sendAfter(Transportation t ) {
		t.transport();
	}
}
