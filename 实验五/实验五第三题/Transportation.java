
public abstract class Transportation {
	private String no;
	private String type;
	private String name;
	public Transportation() {
		
	}
	public Transportation(String no,String type,String name) {
		this.no=no;
		this.type=type;
		this.name=name;
	}
	public void setNo(String no) {
    	this.no=no;
    } 
    public String getNo(String no) {
    	return no;
    }
    public void setType(String type) {
    	this.type=type;
    } 
    public String getType(String type) {
    	return type;
    }
    public void setName(String name) {
    	this.name=name;
    } 
    public String getCno(String name) {
    	return name;
    }
    public abstract void transport();
}
