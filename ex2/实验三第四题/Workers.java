
class Workers {

	private String number;
	private String name;
	private String sex;
	private Department departmentname;
	private Date birthday;
	private Date workdate;
	public Workers () {
	}
	public Workers(String number,String name,String sex) {
		this.number=number;
		this.name=name;
		this.sex=sex;
	}
	public void setnumber(String number) {//职工号
		this.number=number;
	}
	public String getnumber() {
		return number;
	}
	public void setname(String name) {//姓名
		this.name=name;
	}
	public String getname() {
		return name;
	}
	public void setsex(String sex) {//性别
		this.sex=sex;
	}
	public String getsex() {
		return sex;
	}
	public void setbirthday(Date birthday) {//生日
		this.birthday=birthday;
	}
	public Date getbirthday() {
		return birthday;
	}
	public void setdepartmentname(Department departmentname) {//工作部门
		this.departmentname=departmentname;
	}
	public Department getdepartment() {
		return departmentname;
	}
	public void  setworkdate(Date workdate) {//工作日期
		this.workdate=workdate;
	} 
	public Date getworkdate() {
		return workdate;
	}
	public String toString() {
		return "职工号："+number+"姓名:"+name+"性别:"+sex+"生日"+birthday+departmentname+"参加工作时间:"+workdate;
	}
}
