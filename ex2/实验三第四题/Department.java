class Department {
	private  String departmentnumber;
	private  String departmentname;
	private  String post;
	public Department() {
		
	}
	public Department(String departmentnumber) {
		this.departmentnumber=departmentnumber;
	}
	public void setdepartmentnumber(String departmentnumber) {//部门编号
		this.departmentnumber=departmentnumber;
	}
	public String getDepartmentnumber() {
		return departmentnumber;
	}
	public String getDepartmentname() {//部门名称
		return departmentname;
	}
	public void setDepartmentname(String departmentname) {
		this.departmentname = departmentname;
	}
	public void setPost(String  post) {//经理
		this.post = post;	
	}
	public String getPost() {
		return post;
	}
	public String toString() {
		return "部门编号"+departmentnumber+"部门名称："+departmentname;
	}
	public String TOString() {
		return "部门编号:"+departmentnumber+"部门名称："+departmentname+"\n 经理"+post;
	}
}
