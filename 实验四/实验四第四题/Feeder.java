
public class Feeder {
	public static void main(String[] arsg){	
		final String NAME="小李";
		Lion[] lion=new Lion[1];
		lion[0]=new Lion();
		System.out.println(NAME+"给");
		feedAnimal(lion);
		Monkey[] monkey=new Monkey[5];
		System.out.println(NAME+"给");
		for(int i=0;i<monkey.length;i++){
			monkey[i]=new Monkey();
		}
		feedAnimal(monkey);
		Dove[] dove=new Dove[10];
		System.out.println(NAME+"给");
		for(int i=0;i<dove.length;i++){
			dove[i]=new Dove();
		}
		feedAnimal(dove);
	}
	public static void feedAnimal(Animal[] animal){		
		for(int i=0;i<animal.length;i++){
			animal[i].eat();
		}
	}
}
